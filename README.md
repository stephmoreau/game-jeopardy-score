## Jeopardy Scorekeeper  ##
*Only Html and CSS were used to create this scorekeeper.*
Currently in an HTML/CSS mood and want to attempt this scorekeeper based on the Jeopardy TV show. Jeopardy the TV show is a game show where 3 contestants attempt to answer question (in the form of a question) from  6 Categories, each with 5 questions of increasing monetary value. Scoring also in place subtracting on wrong answers and adding when correct.

---

## Extra files used
List of files besides out index.html and style.css
1. Google Fonts to make it look a little nicer (used 3 random fonts for the contestants names)
2. Font Swiss911 XCm BT (https://www.wfonts.com/font/swiss911-xcm-bt)
3. Font Gyparody (https://www.dafont.com/gyparody.font)
4. Font Univers LT Std Black (https://www.download-free-fonts.com/details/72778/univers-lt-std-75-black)

---

## JavaScript
If I had include JavScript, what would be different?
- Everything would automated with variables and functions
- Could allow for dyamic user names
- Could create popup with dynamic questions
---

## Future Tasks ##
Notes on possible options/functionality that could be added in HTML or CSS

1. Double Jeopardy
2. Final Jeopardy (possible?)
